from django.contrib import admin

from mailing_service.models import Broadcast, Client, Message


@admin.register(Broadcast)
class BroadcastAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Broadcast._meta.fields]


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Client._meta.fields]


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Message._meta.fields]
