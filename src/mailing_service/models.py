from django.core.validators import RegexValidator
from django.db import models


class Broadcast(models.Model):
    launch_datetime = models.DateTimeField()
    clients = models.ManyToManyField('Client', related_name='broadcasts')
    message_text = models.TextField()
    end_datetime = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)


class Client(models.Model):
    phone_number = models.CharField(
        max_length=12,
        validators=[
            RegexValidator(
                regex=r'^7\d{10}$',
                message='Phone number must be in the format 7XXXXXXXXXX'
            )
        ]
    )
    mobile_operator_code = models.CharField(max_length=20)
    tag = models.CharField(max_length=100)
    timezone = models.CharField(max_length=100)


class Message(models.Model):
    SHIPPING_STATUS_CHOICES = [
        ('not_sent', 'Not Sent'),
        ('delivered', 'Delivered'),
        ('sending', 'Sending'),
        ('delivery_error', 'Delivery Error'),
        ('queue', 'In Queue')
    ]
    creation_datetime = models.DateTimeField(auto_now_add=True)
    shipping_status = models.CharField(max_length=100, choices=SHIPPING_STATUS_CHOICES, default='not_sent')
    broadcast = models.ForeignKey(Broadcast, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
