from django.db.models import Count
from rest_framework import serializers

from mailing_service.models import Client, Message, Broadcast


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class BroadcastSerializer(serializers.ModelSerializer):
    message_count = serializers.SerializerMethodField()
    status_statistics = serializers.SerializerMethodField()

    class Meta:
        model = Broadcast
        fields = '__all__'

    @staticmethod
    def get_message_count(obj):
        return obj.message_set.count()

    @staticmethod
    def get_status_statistics(obj):
        status_counts = obj.message_set.values('shipping_status').annotate(count=Count('id'))
        return status_counts
