import logging

import pytz
import requests
from celery import shared_task
from datetime import timedelta

from django.utils import timezone

from config import settings
from mailing_service.models import Broadcast, Message, Client

logger = logging.getLogger(__name__)

JWT_TOKEN = settings.PROBE_API_TOKEN
API_URL = settings.PROBE_API_BASE_URL


@shared_task
def send_broadcast():
    """
    To regularly send messages to clients at a specific time, if the current time is within a given time interval.
    """
    current_time = timezone.now()
    one_hour_ago = current_time - timedelta(hours=1)
    broadcasts = Broadcast.objects.filter(created_at__gt=one_hour_ago)
    for broadcast in broadcasts:
        if current_time >= broadcast.launch_datetime and current_time < broadcast.end_datetime:
            headers = {
                "Authorization": f"Bearer {JWT_TOKEN}",
                "Content-Type": "application/json"
            }
            data = []
            for client in broadcast.clients.all():
                client_timezone = pytz.timezone(client.timezone)
                data.append({
                    "client_id": client.pk,
                    "phone": client.phone_number,
                    "text": broadcast.message_text,
                    "timezone": client_timezone,
                })
            for item in data:
                try:
                    client_time = current_time.astimezone(item['timezone'])
                    if client_time >= broadcast.launch_datetime and client_time < broadcast.end_datetime:
                        response = requests.post(
                            f"{API_URL}/{broadcast.id}", json={
                                "id": item['client_id'],
                                "phone": item['phone'],
                                "text": item['text'],
                            }, headers=headers)
                        response.raise_for_status()
                        logger.info("Message sent successfully!")
                        message = Message.objects.create(
                            shipping_status='delivered',
                            broadcast=broadcast,
                            client=Client.objects.get(pk=item['client_id']),
                        )
                    else:
                        logger.info(f"Broadcast for the client {item['client_id']} should not be running at this time.")
                except requests.exceptions.RequestException as e:
                    logger.error("Error sending message.")
                    logger.exception(e)
                    message = Message.objects.create(
                        shipping_status='delivery_error',
                        broadcast=broadcast,
                        client=Client.objects.get(pk=item['client_id']),
                    )
                except Exception as e:
                    logger.error("An error has occurred.")
                    logger.exception(e)
        else:
            logger.info("The broadcast for this time should not be running.")
