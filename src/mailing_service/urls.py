from django.urls import path

from mailing_service.views import \
    ClientListCreateView, \
    ClientRetrieveUpdateDestroyView, \
    BroadcastListCreateView, \
    BroadcastDetailView, \
    BroadcastStatisticsView

urlpatterns = [
    path('clients/', ClientListCreateView.as_view(), name='client-list-create'),
    path('clients/<int:pk>/', ClientRetrieveUpdateDestroyView.as_view(), name='client-retrieve-update-destroy'),

    path('broadcasts/', BroadcastListCreateView.as_view(), name='broadcast-list-create'),
    path('broadcasts/<int:pk>/', BroadcastDetailView.as_view(), name='broadcast-detail'),
    path('broadcasts/statistics/', BroadcastStatisticsView.as_view(), name='broadcast-statistics'),
    path('broadcasts/<int:pk>/statistics/', BroadcastStatisticsView.as_view(), name='broadcast-statistics'),
]
