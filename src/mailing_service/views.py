from django.db.models import Count
from rest_framework import generics

from mailing_service.models import Client, Broadcast
from mailing_service.serializers import ClientSerializer, BroadcastSerializer


class ClientListCreateView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class BroadcastListCreateView(generics.ListCreateAPIView):
    queryset = Broadcast.objects.all()
    serializer_class = BroadcastSerializer


class BroadcastDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Broadcast.objects.all()
    serializer_class = BroadcastSerializer


class BroadcastStatisticsView(generics.ListAPIView):
    queryset = Broadcast.objects.annotate(message_count=Count('message')).prefetch_related('message_set')
    serializer_class = BroadcastSerializer
